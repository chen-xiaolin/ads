package io.finer.ads.jeecg.service;

import io.finer.ads.jeecg.entity.ShowLog;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * @Description: 呈现日志-最新
 * @Author: jeecg-boot
 * @Date:   2020-06-16
 * @Version: V1.0
 */
public interface IShowLogService extends IService<ShowLog> {

}
