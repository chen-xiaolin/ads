const Joi = require('joi');
const Controllers = require('../controllers');

let getSlot = {
    method: 'get',
    path: '/slot',
    config: {
        auth: false,
        description: 'Routing with parameters',
        notes: 'slot api',
        tags: ['api'],
        validate: {
            query: {
                id: Joi.required(),
            }
        },
        id: 'getSlot'
    },
    handler: Controllers.slot.getSlot
};

let getAds = {
    method: 'get',
    path: '/slot/ads',
    config: {
        auth: false,
        description: 'Routing with parameters',
        notes: 'slot api',
        tags: ['api'],
        validate: {
            query: {
                id: Joi.required(),
            }
        },
        id: 'getAds'
    },
    handler: Controllers.slot.getAds
};
module.exports = [getSlot, getAds];