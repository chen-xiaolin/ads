module.exports = [
  require(__dirname + '/staticfile.js'),
  require(__dirname + '/slot.js'),
  require(__dirname + '/showLog.js'),
  require(__dirname + '/clickLog.js')
]
